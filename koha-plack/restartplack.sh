#!/bin/bash
# This file is part of Koha.
#
# Copyright (C) 2017 Catalyst IT
#
# Koha is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Koha is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Koha; if not, see <http://www.gnu.org/licenses>.

set -e

. /lib/lsb/init-functions

# Read configuration variable file if it is present
[ -r /etc/default/koha-common ] && . /etc/default/koha-common

# include helper functions
if [ -f "/usr/share/koha/bin/koha-functions.sh" ]; then
     . "/usr/share/koha/bin/koha-functions.sh"
else
     echo "Error: /usr/share/koha/bin/koha-functions.sh not present." 1>&2
     exit 1
fi

declare -a zebrainstances
for singleinstancename in $(koha-list --plack --enabled)
do
    if ! is_plack_running $singleinstancename; then
        zebrainstances+=" $singleinstancename"
        warn "The Plack service for instance $singleinstancename is not running."
    else
        warn "Plack service is running for this instance $singleinstancename."
    fi
done

if [ ! -z "$zebrainstances" ];
then
    for zebrainstance in $zebrainstances
  do
     instancename=$zebrainstance
     run=$(koha-plack --start $instancename)
     warn "Starting Plack service for $instancename"
  done
fi
exit 0;

