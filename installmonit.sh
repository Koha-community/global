#!/bin/bash

apt-get install monit

monitpath=/etc/monit
bashpath=/usr/sbin/

cp ./koha-plack/plack.conf $monitpath/conf.d/plack.conf
cp ./koha-zebra/zebra.conf $monitpath/conf.d/zebra.conf
cp ./koha-coce/coce.conf $monitpath/conf.d/coce.conf
cp ./koha-sip/sip.conf $monitpath/conf.d/sip.conf
cp ./koha-zebra/*.sh $bashpath/
cp ./koha-plack/*.sh $bashpath/
cp ./koha-coce/*.sh $bashpath/
cp ./koha-sip/*.sh $bashpath/

echo "What mailserver should we send through?"
read mailserver
echo "Where should we send the emails?"
read monitmail

echo "
set mailserver $mailserver,
set alert $monitmail not on { instance, action }
set httpd port 2812
" > $monitpath/conf.d/koha-common.conf

service monit start
monit start all
